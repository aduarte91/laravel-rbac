<?php

return [
    /*
     * User class to be included with the RBAC functionality
     */
    'user_class' => \App\User::class,

    /*
     * Role that will have no restrictions for access the whole application
     */
    'admin_role_name' => 'super-admin',

    /*
     * The id increments by in the database
     */
    'id_multiplier' => 1
];
