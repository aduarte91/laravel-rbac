<?php

namespace Govzilla\RBAC\AccessControl;

use App\User;
use Govzilla\RBAC\Models\Component;
use Govzilla\RBAC\Models\Group;
use Govzilla\RBAC\Models\GroupPermission;
use Govzilla\RBAC\Models\Module;
use Govzilla\RBAC\Models\Permission;
use Govzilla\RBAC\Models\Role;
use Govzilla\RBAC\Models\RoleGroupPermission;
use Highlight\Mode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AccessControl
{
    /**
     * @param $user
     * @param string $permission
     * @param string $module
     * @param string|null $component
     * @return bool
     */
    public function canAccess ($user, string $permission, string $module, string $component) : bool
    {
        try {
            $userClass = config('rbac.user_class');
            throw_unless($user instanceof $userClass,
                \Exception::class, "User must be an instance of $userClass");
        } catch (\Throwable $e) {
            return false;
        }

        try {
            if ($user->isAdministrator) {
                return true;
            } else {
                $module = Module::where('name', $module)
                    ->firstOrFail();
                $component = Component::where('name', $component)
                    ->firstOrFail();

                $group = Group::where([
                    'module_id' => $module->id,
                    'component_id' => $component->id
                ])->with(['permissions' => function ($query) use ($permission) {
                    $query->where('name', $permission)
                    ->firstOrFail();
                }])->firstOrFail();

                $groupPermission = $group->permissions->first()->pivot;

                $user->role
                     ->roleGroupPermissions()
                     ->where('group_permission_id', $groupPermission->id)
                     ->firstOrFail();

                return true;
            }
        } catch (ModelNotFoundException $exception) {
            return false;
        }
    }

    /**
     * @param $user
     * @return Model
     */
    public function allAccess ($user) : Model
    {
        try {
            $userClass = config('rbac.user_class');
            throw_unless($user instanceof $userClass,
                \Exception::class, "User must be an instance of $userClass");
        } catch (\Throwable $e) {
            return null;
        }

        return $userClass::where('id', $user->id)
            ->with(['role.roleGroupPermissions'])
            ->first();
    }

    /**
     * @param string $role
     * @param string $permission
     * @param string $module
     * @param string $component
     * @return RoleGroupPermission
     */
    public function assignPrivilegeByRole(
        string $role,
        string $permission,
        string $module,
        string $component
    ) : RoleGroupPermission {
        $groupPermission = $this->assignPermissionByGroup($permission, $module, $component);
        $role = Role::where('name', $role)->first();

        $record = RoleGroupPermission::withTrashed()->firstOrCreate([
            'group_permission_id' => $groupPermission->id,
            'role_id' => $role->id
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param string $role
     * @param string $permission
     * @param string $module
     * @param string $component
     * @throws \Exception
     */
    public function removePrivilegeByRole(
        string $role,
        string $permission,
        string $module,
        string $component
    ) {
        $this->getRoleGroupPermission($role, $permission, $module, $component)
             ->delete();
    }

    /**
     * @param int $roleId
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @return RoleGroupPermission
     */
    public function assignPrivilegeByRoleByIds(
        int $roleId,
        int $permissionId,
        int $moduleId,
        int $componentId
    ) : RoleGroupPermission {
        $groupPermission = $this->assignPermissionByGroupByIds($permissionId, $moduleId, $componentId);

        $record = RoleGroupPermission::withTrashed()->firstOrCreate([
            'role_id' => $roleId,
            'group_permission_id' => $groupPermission->id
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param int $roleId
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @throws \Exception
     */
    public function removePrivilegeByRoleByIds(
        int $roleId,
        int $permissionId,
        int $moduleId,
        int $componentId
    ) {
        $this->getRoleGroupPermissionByIds($roleId, $permissionId, $moduleId, $componentId)
             ->delete();
    }

    /**
     * @param string $permission
     * @param string $module
     * @param string $component
     * @return GroupPermission
     */
    public function assignPermissionByGroup(
        string $permission,
        string $module,
        string $component
    ) : GroupPermission {
        $group = $this->assignGroup($module, $component);
        $permission = Permission::where('name', $permission)->first();

        $record = GroupPermission::withTrashed()->firstOrCreate([
            'group_id' => $group->id,
            'permission_id' => $permission->id
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param string $permission
     * @param string $module
     * @param string $component
     * @throws \Exception
     */
    public function removePermissionByGroup(
        string $permission,
        string $module,
        string $component
    ) {
        $this->getGroupPermission($permission, $module, $component)
             ->delete();
    }

    /**
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @return GroupPermission
     */
    public function assignPermissionByGroupByIds(
        int $permissionId,
        int $moduleId,
        int $componentId
    ) : GroupPermission {
        $group = $this->assignGroupByIds($moduleId, $componentId);

        $record = GroupPermission::withTrashed()->firstOrCreate([
            'group_id' => $group->id,
            'permission_id' => $permissionId
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @throws \Exception
     */
    public function removePermissionByGroupByIds(
        int $permissionId,
        int $moduleId,
        int $componentId
    ) {
        $this->getGroupPermissionByIds($permissionId, $moduleId, $componentId)
             ->delete();
    }

    /**
     * @param string $module
     * @param string $component
     * @return Group
     */
    public function assignGroup(
        string $module,
        string $component
    ) : Group {
        $module = Module::where('name', $module)->firstOrFail();
        $component = Component::where('name', $component)->firstOrFail();

        $record = Group::withTrashed()->firstOrCreate([
            'module_id' => $module->id,
            'component_id' => $component->id
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param string $module
     * @param string $component
     * @throws \Exception
     */
    public function removeGroup(
        string $module,
        string $component
    ) {
        $this->getGroup($module, $component)
             ->delete();
    }

    /**
     * @param int $moduleId
     * @param int $componentId
     * @return Group
     */
    public function assignGroupByIds(
        int $moduleId,
        int $componentId
    ) : Group {
        $record = Group::withTrashed()->firstOrCreate([
            'module_id' => $moduleId,
            'component_id' => $componentId
        ]);

        if ($record->trashed()) {
            $record->restore();
            $record->save();
        }

        return $record;
    }

    /**
     * @param int $moduleId
     * @param int $componentId
     * @throws \Exception
     */
    public function removeGroupByIds(
        int $moduleId,
        int $componentId
    ) {
        $this->getGroupByIds($moduleId, $componentId)
             ->delete();
    }

    /**
     * @param string $role
     * @param string $permission
     * @param string $module
     * @param string $component
     * @return RoleGroupPermission
     */
    private function getRoleGroupPermission(
        string $role,
        string $permission,
        string $module,
        string $component
    ) : RoleGroupPermission {
        $groupPermission = $this->getGroupPermission($permission, $module, $component);
        $role = Role::where('name', $role)->first();

        return RoleGroupPermission::where([
            'group_permission_id' => $groupPermission->id,
            'role' => $role->id
        ])->first();
    }

    /**
     * @param string $permission
     * @param string $module
     * @param string $component
     * @return GroupPermission
     */
    private function getGroupPermission(
        string $permission,
        string $module,
        string $component
    ) : GroupPermission {
        $group = $this->getGroup($module, $component);
        $permission = Permission::where('name', $permission)->first();

        return GroupPermission::where([
            'group_id' => $group->id,
            'permission_id' => $permission->id
        ])->first;
    }

    /**
     * @param string $module
     * @param string $component
     * @return Group
     */
    private function getGroup(string $module, string $component) : Group
    {
        $module = Module::where('name', $module)
            ->firstOrFail();
        $component = Component::where('name', $component)
            ->firstOrFail();
        return Group::where([
            'module_id' => $module->id,
            'component_id' => $component->id
        ])->first();
    }

    /**
     * @param int $roleId
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @return RoleGroupPermission
     */
    private function getRoleGroupPermissionByIds(
        int $roleId,
        int $permissionId,
        int $moduleId,
        int $componentId
    ) : RoleGroupPermission {
        $groupPermission = $this->getGroupPermissionByIds($permissionId, $moduleId, $componentId);

        return RoleGroupPermission::where([
            'role_id' => $roleId,
            'group_permission_id' => $groupPermission->id
        ])->first();
    }

    /**
     * @param int $permissionId
     * @param int $moduleId
     * @param int $componentId
     * @return GroupPermission
     */
    private function getGroupPermissionByIds(
        int $permissionId,
        int $moduleId,
        int $componentId
    ) : GroupPermission {
        $group = $this->getGroupByIds($moduleId, $componentId);

        return GroupPermission::where([
           'group_id' => $group->id,
           'permission_id' => $permissionId
        ])->first();
    }

    /**
     * @param int $moduleId
     * @param int $componentId
     * @return Group
     */
    private function getGroupByIds(int $moduleId, int $componentId) : Group
    {
        return Group::where([
            'module_id' => $moduleId,
            'component_id' => $componentId
        ])->first();
    }
}
