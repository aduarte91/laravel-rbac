<?php

namespace Govzilla\RBAC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleGroupPermission extends Model
{
    const UPDATED_AT = null;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_group_permission';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'group_permission_id'
    ];

    /**
     * Get the group permission for the role_group_permission.
     */
    public function groupPermission()
    {
        return $this->belongsTo(GroupPermission::class);
    }

    /**
     * Get the role for the role_group_permission.
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
