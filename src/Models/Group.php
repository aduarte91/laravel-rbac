<?php

namespace Govzilla\RBAC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    const UPDATED_AT = null;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'module_id',
        'component_id'
    ];

    /**
     * Get the module for the group.
     */
    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    /**
     * Get the component for the group.
     */
    public function component()
    {
        return $this->belongsTo(Component::class);
    }

    /**
     * Get the group_permissions for the group.
     */
    public function groupPermissions()
    {
        return $this->hasMany(GroupPermission::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,
            'group_permission',
            'group_id',
            'permission_id'
        );
    }
}
