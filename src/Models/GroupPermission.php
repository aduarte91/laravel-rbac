<?php

namespace Govzilla\RBAC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupPermission extends Model
{
    const UPDATED_AT = null;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group_permission';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'permission_id',
        'group_id'
    ];

    /**
     * Get the permission for the group_permission.
     */
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    /**
     * Get the group for the group_permission.
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Get the role_group_permission for the group_permission.
     */
    public function roleGroupPermissions()
    {
        return $this->hasMany(RoleGroupPermission::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_group_permission',
            'group_permission_id',
            'role_id'
        );
    }
}
