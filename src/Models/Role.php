<?php

namespace Govzilla\RBAC\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    const UPDATED_AT = null;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name'
    ];

    /**
     * Get the user roles for the role.
     */
    public function users()
    {
        $userClass = config('rbac.user_class');
        return $this->hasMany($userClass);
    }

    /**
     * Get the role role group permissions for the role.
     */
    public function roleGroupPermissions()
    {
        return $this->hasMany(RoleGroupPermission::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groupPermissions()
    {
        return $this->belongsToMany(
            GroupPermission::class,
            'role_group_permission',
            'role_id',
            'group_permission_id'
        );
    }
}
