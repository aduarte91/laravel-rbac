<?php

namespace Govzilla\RBAC;

use Govzilla\RBAC\Middlewares\VerifyRbacAccess;
use Illuminate\Support\ServiceProvider;

class LaravelRbacServiceProvider extends ServiceProvider
{
    public function boot()
    {
        /*
         * Publishes config files
         */
        $this->publishes([
            __DIR__ . '/../config/rbac.php' => config_path('rbac.php'),
        ], 'rbac-config');

        /*
         * Publishes factories files
         */
        $this->publishes([
            __DIR__ . '/../database/factories' => database_path('factories/'),
        ], 'rbac-factories');

        /*
         * Publishes seeders files
         */
        $this->publishes([
            __DIR__ . '/../database/seeds'
            => database_path('seeds'),
        ], 'rbac-seeds');

        /*
         * Loads migration files
         */
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    public function register()
    {
        /*
         * Register the RBAC facade
         */
        $this->app->bind('rbac', function ($app) {
            $user = auth()->user();
            return (new AccessControl\AccessControl($user));
        });

        /*
         * Register the middleware
         */
        app('router')->aliasMiddleware('verify-rbac-access', VerifyRbacAccess::class);
    }
}
