<?php

namespace Govzilla\RBAC;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CanAccessDirectiveServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('canAccess', function ($permission, $module, $component) {
            $canAccess = false;

            if (Auth::check()) {
                $canAccess = Auth::user()->canAccess($permission, $module, $component);
            }

            return $canAccess;
        });
    }
}
