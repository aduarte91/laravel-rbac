<?php
namespace Govzilla\RBAC\Middlewares;

use Illuminate\Support\Facades\Auth;
use Closure;

class VerifyRbacAccess
{
    /**
     * @param $request
     * @param Closure $next
     * @param mixed ...$params
     * @return Closure
     */
    public function handle($request, Closure $next, ...$params)
    {
        list($permission, $module, $component) = $params;

        $user = Auth::user();

        if (is_null($user)) {
            return redirect('/');
        }

        if (!$user->canAccess($permission, $module, $component)) {
            return abort(403, "You don't have access to this page");
        }

        return $next($request);
    }
}
