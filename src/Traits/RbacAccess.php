<?php
namespace Govzilla\RBAC\Traits;

use Govzilla\RBAC\Facades\RBAC;
use Govzilla\RBAC\Models\Role;
use Highlight\Mode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait RbacAccess
{
    /**
     * @param string $permission
     * @param string $module
     * @param string $component
     * @return bool
     */
    public function canAccess(
        string $permission,
        string $module,
        string $component
    ) : bool {
        return RBAC::canAccess($this, $permission, $module, $component);
    }

    /**
     * @return bool
     */
    public function getIsAdministratorAttribute(): bool
    {
        $role = $this->role;
        return $role->name === config('rbac.admin_role_name');
    }

    /**
     * Role the user belongs to
     * @return BelongsTo
     */
    public function role() : BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    /**
     * @return Model
     */
    public function allAccess() : Model
    {
        return RBAC::allAccess($this);
    }
}
