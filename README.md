# Laravel-RBAC

## Introduction

Laravel-RBAC is an easy to use package working with laravel to manage granular permissions based in user's role.
It currently supports:

*  Be used as a blade directive.
*  Be used as a middleware.
*  Access it througth a Facade.
*  Give and revoke granular permissions to a user's role.
*  Give and remove permissions to a group.
*  Group by modules and components.

## Database Model
![alt text](public/images/rbac-model.png "Database Model")

### Tables definition

| Table name | Definition |
|---|---|
| roles | This table defines the user's role to give a set of permission to users. |
| role_group_permission | The intersecction between roles, groups and permission. This determines the permissions that a role has. |
| group_permission | The intersecction between groups and permissions. This assign permissions to the groups. |
| permissions | Permission that will be assign to groups (Create, Read, Update, Delete, Etc.) |
| groups | The grouping between modules and components. |
| modules | The modules of the application, a module can have multiple components. |
| components | The components of the application. A component can belong to multiple modules. |


## Installation

#### Add the project repository to your composer.json 
To include the project repository to your composer.json execute the command: 

With SSH:

    composer config repositories.laravel-rbac vcs git@bitbucket.org:liliatirado/laravel-rbac.git  

With HTTPS

    composer config repositories.laravel-rbac vcs https://liliatirado@bitbucket.org/liliatirado/laravel-rbac.git

#### Add the project to the composer.json require:

    "require": {  
       ...
        "govzilla/laravel-rbac": "^0.1.12",
       ...  
    }

#### Install the package

To install the project:

    composer update

To discover the package:

    composer dump-autoload

## Configuration

After installing the package, it's required to add some configuration to your application, for the package to work.

#### Publish package files
Run the command

    php artisan vendor:publish

and select the files to be published.

*  To publish everything select:

![alt text](public/images/rbac-publish-all.png "Select all the provider files")

*  Or to publish the files individually select:

![alt text](public/images/rbac-tags.png "Select a single tag")

>*It is **required** to pusblish the **config files**.*

#### Set the config variables

![alt text](public/images/rbac-config.png "Config file")

#### Seeders
Optionally, you can configure the seeds as you need them. Remember that the project does not have the ability to create a new module or component by code.

To use the RBAC seeders:

*  Be sure to publish the **rbac-seeds**.
*  Run the composer autoloader command 
    *  `composer dump-autoload`  
*  Add the seeders into the DatabaseSeederClass in the following order:

```php
public function run()
{
	$this->call(ComponentsTableSeeder::class);
	$this->call(ModulesTableSeeder::class);`  
	$this->call(RolesTableSeeder::class);  
	$this->call(PermissionsTableSeeder::class);  
	$this->call(GroupsTableSeeder::class);  
	$this->call(GroupPermissionTableSeeder::class);  
	$this->call(RoleGroupPermissionTableSeeder::class);
}
```

>**NOTE**: Make sure to add the seeders in this way to avoid constraints exceptions.

#### Run the migrations
To run the migrations without seeding the database run:

    php artisan migrate

To run the migrations seeding the database run:

    php artisan migrate --seed

#### Add the trait to your users class
For rbac to work you need to add the **RbacAccess** trait to your users model class.

![alt text](public/images/rbac-trait.png "RbacAccess Trait")

>Aditionally, you can add **role_id - integer** to your users model attributes ($fillable, $casts, etc.)

## Usage

As metioned before, the package includes a middleware, a blade directive and a Facade to access the methods.

#### User trait
Name: RbacAccess

Functions:

*  **canAccess()**: Returns if the user has access or not to the given parameters.
	*  Permission: string, required.
	*  Module: string, required.
	*  Component: string, required.
*  **allAccess()**: Returns all the user access.

Example:

    $user->canAccess(Permisson, Module, Submodule);
    $user->allAccess();

#### Middleware
Name: verify-rbac-access

Params: 

*  Permission: string, required.
*  Module: string, required.
*  Component: string, required.

Example:

    Route::get('/')->middleware('verify-rbac-access:Permission,Module,Component')

#### Blade directive
Name: @canAccess()

Params:

*  Permission: string, required.
*  Module: string, required.
*  Component: string, required.

Example:

    @canAccess(Permission, Module, Component)

#### Facade
Name: RBAC

Functions:
 
*  **canAccess**: Returns if a user has a permission to a module and component.
	*  user: users model instance, required.
	*  permission: string, required.
	*  module: string, required.
	*  component: string, required.
*  **allAccess**: Returns all the user permissions defined by its role.
	*  user: users model instance, required.
*  **assignPrivilegeByRole**: Assign a privilege to a role.
	*  role: string, required.
	*  permission: string, required.
	*  module: string, required.
	*  component: string, required.
*  **removePrivilegeByRole**: Remove a privilege to a role.
	*  role: string, required.
	*  permission: string, required.
	*  module: string, required.
	*  component: string, required.
*  **assignPrivilegeByRoleByIds**: Assign a privilege to a role using ids.
	*  roleId: integer, required.
	*  permissionId: integer, required.
	*  moduleId: integer, required.
	*  componentId: integer, required.
*  **removePrivilegeByRoleByIds**: Remove a privilege to a role using ids.
	*  roleId: integer, required.
	*  permissionId: integer, required.
	*  moduleId: integer, required.
	*  componentId: integer, required.
*  **assignPermissionByGroup**: Assign permission to a group.
	*  permission: string, required.
	*  module: string, required.
	*  component: string, required.
*  **removePermissionByGroup**: Remove permission to a group.
	*  permission: string, required.
	*  module: string, required.
	*  component: string, required.
*  **assignPermissionByGroupByIds**:  Assign permission to a group using ids.
	*  permissionId: integer, required.
	*  moduleId: integer, required.
	*  componentId: integer, required.
*  **removePermissionByGroupByIds**: Remove permission to a group using ids.
	*  permissionId: integer, required.
	*  moduleId: integer, required.
	*  componentId: integer, required.
*  **assignGroup**: Creates a new group.
	*  module: string, required.
	*  component: string, required.
*  **removeGroup**: Deletes a group.
	*  module: string, required.
	*  component: string, required.
*  **assignGroupByIds**: Creates a new group using ids.
	*  moduleId: integer, required.
	*  componentId: integer, required.
*  **removeGroupByIds**: Deletes a group using ids.
	*  moduleId: integer, required.
	*  componentId: integer, required.
