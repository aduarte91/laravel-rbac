<?php

use Govzilla\RBAC\Models\Group;
use Govzilla\RBAC\Models\Permission;
use Govzilla\RBAC\Models\GroupPermission;
use Illuminate\Database\Seeder;

class GroupPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();
        $groups = Group::all();

        $groups->each(function ($group) use ($permissions) {
            $permissions->each(function ($permission) use ($group){
                factory(GroupPermission::class, 1)->create([
                    'group_id' => $group->id,
                    'permission_id' => $permission->id
                ]);
            });
        });
    }
}
