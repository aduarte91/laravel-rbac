<?php

use Govzilla\RBAC\Models\Role;
use Govzilla\RBAC\Models\RoleGroupPermission;
use Govzilla\RBAC\Models\GroupPermission;
use Illuminate\Database\Seeder;

class RoleGroupPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', '<>', 'super-admin')->first();
        $groupPermissions = GroupPermission::all();

        $groupPermissions->each(function ($groupPermission) use ($role) {
                factory(RoleGroupPermission::class, 1)->create([
                    'role_id' => $role->id,
                    'group_permission_id' => $groupPermission->id
                ]);
        });
    }
}
