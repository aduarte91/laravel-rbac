<?php

use Govzilla\RBAC\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Permission::class, 1)->create([
            'name' => 'create',
            'display_name' => 'Create'
        ]);

        factory(Permission::class, 1)->create([
            'name' => 'read',
            'display_name' => 'Read',
        ]);

        factory(Permission::class, 1)->create([
            'name' => 'update',
            'display_name' => 'Update',
        ]);

        factory(Permission::class, 1)->create([
            'name' => 'delete',
            'display_name' => 'Delete',
        ]);
    }
}
