<?php

use Govzilla\RBAC\Models\Group;
use Govzilla\RBAC\Models\Module;
use Govzilla\RBAC\Models\Component;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = Module::where('name', 'enforcement-analytics')->first();
        $downloadComponent = Component::where('name', 'download-button')->first();
        $requestComponent = Component::where('name', 'request-button')->first();

        factory(Group::class, 1)->create([
            'module_id' => $module->id,
            'component_id' => $downloadComponent->id
        ]);

        factory(Group::class, 1)->create([
            'module_id' => $module->id,
            'component_id' => $requestComponent->id
        ]);
    }
}
