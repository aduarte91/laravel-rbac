<?php

use Govzilla\RBAC\Models\Component;
use Illuminate\Database\Seeder;

class ComponentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Component::class, 1)->create([
            'name' => 'download-button',
            'display_name' => 'Download Button'
        ]);

        factory(Component::class, 1)->create([
            'name' => 'request-button',
            'display_name' => 'Request Button',
        ]);
    }
}
