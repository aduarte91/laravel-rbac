<?php

use Govzilla\RBAC\Models\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Module::class, 1)->create([
            'name' => 'enforcement-analytics',
            'display_name' => 'Enforcement Analytics'
        ]);

        factory(Module::class, 1)->create([
            'name' => 'newsfeed',
            'display_name' => 'Newsfeed',
        ]);
    }
}
