<?php

use Govzilla\RBAC\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the roles seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Role::class, 1)->states('Super Admin')->create();
        factory(Role::class, 1)->states('Client')->create();
    }
}
