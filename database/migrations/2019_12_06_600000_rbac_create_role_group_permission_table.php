<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RbacCreateRoleGroupPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_group_permission', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('group_permission_id');
            $table->dateTime('created_at');
            $table->softDeletes();

            $table->unique(['group_permission_id', 'role_id']);

            $table->foreign('group_permission_id', 'fk_group_permission')
                ->references('id')
                ->on('group_permission');
            $table->foreign('role_id', 'fk_roles')
                ->references('id')
                ->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_group_permission', function (Blueprint $table) {
            $table->dropForeign('fk_group_permission');
            $table->dropForeign('fk_roles');
        });
        Schema::dropIfExists('role_group_permission');
    }
}
