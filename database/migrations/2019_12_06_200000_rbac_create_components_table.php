<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RbacCreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->string('name')->unique();
            $table->string('display_name')->unique();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->dateTime('created_at');
            $table->softDeletes();

            $table->foreign('parent_id', 'fk_components_parent')
                ->references('id')
                ->on('components');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('components', function (Blueprint $table) {
            $table->dropForeign('fk_components_parent');
        });
        Schema::dropIfExists('components');
    }
}
