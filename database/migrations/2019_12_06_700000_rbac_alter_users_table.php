<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RbacAlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userClass = config('rbac.user_class');
        $user = new $userClass;
        $userTableName = $user->getTable();

        Schema::table($userTableName, function (Blueprint $table) {
            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id', 'fk_user_roles')
                ->references('id')
                ->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $userClass = config('rbac.user_class');
        $user = new $userClass;
        $userTableName = $user->getTable();

        Schema::table($userTableName, function (Blueprint $table) {
            $table->dropForeign('fk_user_roles');
        });
    }
}
