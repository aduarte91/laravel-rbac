<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RbacCreateGroupPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_permission', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('permission_id');
            $table->dateTime('created_at');
            $table->softDeletes();

            $table->unique(['group_id', 'permission_id']);

            $table->foreign('group_id', 'fk_groups')
                ->references('id')
                ->on('groups');
            $table->foreign('permission_id', 'fk_permissions')
                ->references('id')
                ->on('permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_permission', function (Blueprint $table) {
            $table->dropForeign('fk_groups');
            $table->dropForeign('fk_permissions');
        });
        Schema::dropIfExists('group_permission');
    }
}
