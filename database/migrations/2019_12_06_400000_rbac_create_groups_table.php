<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RbacCreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('module_id');
            $table->unsignedBigInteger('component_id');
            $table->dateTime('created_at');
            $table->softDeletes();

            $table->unique(['module_id', 'component_id']);

            $table->foreign('module_id', 'fk_modules')
                ->references('id')
                ->on('modules');
            $table->foreign('component_id', 'fk_components')
                ->references('id')
                ->on('components');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign('fk_modules');
            $table->dropForeign('fk_components');
        });
        Schema::dropIfExists('groups');
    }
}
