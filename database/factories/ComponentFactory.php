<?php

use Faker\Generator as Faker;
use Govzilla\RBAC\Models\Component;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Component::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'display_name' => $faker->unique()->name,
    ];
});
