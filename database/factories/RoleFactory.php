<?php

use Faker\Generator as Faker;
use Govzilla\RBAC\Models\Role;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Role::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'display_name' => $faker->unique()->name,
    ];
});

$factory->state(Role::class, 'Super Admin', function () {
    return [
        'name' => 'super-admin',
        'display_name' => 'Super Admin',
    ];
});

$factory->state(Role::class, 'Client', function () {
    return [
        'name' => 'client',
        'display_name' => 'Client',
    ];
});
