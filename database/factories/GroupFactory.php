<?php

use Faker\Generator as Faker;
use Govzilla\RBAC\Models\Group;

$multiplier = config('rbac.id_multiplier');
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Group::class, function (Faker $faker) use ($multiplier){
    return [
        'module_id' => 1 + ($faker->numberBetween(0 ,1) * $multiplier),
        'component_id' => 1 + ($faker->numberBetween(0 ,1) * $multiplier),
    ];
});
