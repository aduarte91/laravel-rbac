<?php

use Faker\Generator as Faker;
use Govzilla\RBAC\Models\RoleGroupPermission;

$multiplier = config('rbac.id_multiplier');
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(RoleGroupPermission::class, function (Faker $faker) use ($multiplier){
    return [
        'role_id' => 1 + ($faker->numberBetween(1, 1) * $multiplier),
        'group_permission_id' => 1 + ($faker->numberBetween(0, 7) * $multiplier),
    ];
});
